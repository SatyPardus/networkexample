# Network Example using Lidgren
*by Kevin Lienkamp*
* * *
## This example shows, what i can do with Networking, using the lidgren library ##
**What does it include?**

1. An Loginserver that handles connections between clients and gameserver
2. An GameServer that holds gamelogic for a simple "tic tac toe" and "four in a row"
3. An Client with simple login, game room and games