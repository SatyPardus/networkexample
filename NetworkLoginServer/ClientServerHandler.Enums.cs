﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public partial class ClientServerHandler
    {
        private enum MessageType : byte
        {
            ClientConnected,
            ClientDisconnected,
            ChatMessage
        }
    }
}
