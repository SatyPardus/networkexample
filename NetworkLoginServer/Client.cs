﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public class Client
    {
        private int _id;
        private string _username;
        private NetConnection _connection;

        public int ID { get { return _id; } }
        public string Username { get { return _username; } }
        public NetConnection Connection { get { return _connection; } }

        public Client(NetConnection connection, int id, string username)
        {
            _id = id;
            _username = username;
            _connection = connection;
        }
    }
}
