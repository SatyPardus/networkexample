﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public partial class GameServerHandler
    {
        public void UpdateServer(GameServer server, int runningGames, int connectedClients)
        {
            server.UpdateServer(runningGames, connectedClients);
        }
    }
}
