﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using System.Threading;

namespace NetworkLoginServer
{
    /****************************** GameServerHandler ******************************\
    What does this class do?
    - Handles the Connection between the Login Server and the Game Server
    - Shares Information of Users to verify connections

    
    \*******************************************************************************/
    public partial class GameServerHandler
    {
        private bool _isRunning = false;
        private bool _shouldStop = false;
        private NetServer _gameServer;
        private ClientServerHandler _clientServerHandler;
        private int _lastID = 1;

        private Dictionary<long, GameServer> _gameServerList = new Dictionary<long, GameServer>();
        private List<int> _portsInUse = new List<int>();

        public bool DebugEnabled = true;
        public bool IsRunning { get { return _isRunning; } }

        public GameServerHandler(ClientServerHandler clientserverhandler)
        {
            _clientServerHandler = clientserverhandler;
        }

        public void Initialize()
        {
            NetPeerConfiguration config = new NetPeerConfiguration("gameserver");
            config.ConnectionTimeout = 10;
            config.Port = 7777;
            config.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);

            _gameServer = new NetServer(config);
            _gameServer.Start();

            new Thread(new ThreadStart(ListenToMessages)).Start();
        }

        private void Reset()
        {
            _shouldStop = false;
            _isRunning = false;
            _gameServer = null;
        }

        private void ListenToMessages()
        {
            _isRunning = true;

            while (!_shouldStop)
            {
                NetIncomingMessage im;
                while ((im = _gameServer.ReadMessage()) != null)
                {
                    // handle incoming message
                    switch (im.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            {
                                if (DebugEnabled)
                                    Log(im.ReadString());
                            }
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            {
                                NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                                string reason = im.ReadString();

                                if (DebugEnabled)
                                    Log(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);

                                if (status == NetConnectionStatus.Connected)
                                {
                                    GameServer server = null;
                                    if (!_gameServerList.TryGetValue(im.SenderConnection.RemoteUniqueIdentifier, out server))
                                    {
                                        im.SenderConnection.Disconnect("Not initialized");
                                        break;
                                    }

                                    _portsInUse.Add(server.Port);
                                    Log("GameServer with id " + server.ID + " connected");
                                }
                                else if (status == NetConnectionStatus.Disconnected)
                                {
                                    GameServer server = null;
                                    if (_gameServerList.TryGetValue(im.SenderConnection.RemoteUniqueIdentifier, out server))
                                    {
                                        _portsInUse.Remove(server.Port);
                                        _gameServerList.Remove(im.SenderConnection.RemoteUniqueIdentifier);
                                        Log("GameServer with id " + server.ID + " disconnected. Reason: " + reason);
                                    }
                                }
                            }
                            break;
                        case NetIncomingMessageType.ConnectionApproval:
                            {
                                /*/
                                /   It is possible to have some kind of verification for servers.
                                /   For this example i skip it and just approve the connection, from any source
                                /
                                /   Possible verification:
                                /   string key = im.ReadString();
                                /   if(allowedKeys.Contains(key))
                                /       im.SenderConnection.Approve();
                                /   else
                                /       im.SenderConnection.Deny();
                                /   **(Just example code)**
                                /*/

                                int port = GetFreePort();
                                int id = _lastID++;

                                _gameServerList.Add(im.SenderConnection.RemoteUniqueIdentifier, new GameServer(id, port));

                                NetOutgoingMessage approveMessage = _gameServer.CreateMessage();
                                approveMessage.Write(port);
                                im.SenderConnection.Approve(approveMessage);
                            }
                            break;
                        case NetIncomingMessageType.Data:
                            {
                                byte msgTypeID = 0;
                                if (im.ReadByte(out msgTypeID))
                                {
                                    GameServer server = null;
                                    if (!_gameServerList.TryGetValue(im.SenderConnection.RemoteUniqueIdentifier, out server))
                                    {
                                        im.SenderConnection.Disconnect("Not initialized");
                                        break;
                                    }

                                    switch ((MessageType)msgTypeID)
                                    {
                                        case MessageType.Update:
                                            int runningGames = im.ReadInt32();
                                            int connectedClients = im.ReadInt32();

                                            UpdateServer(server, runningGames, connectedClients);
                                            break;
                                    }
                                }
                                else
                                {
                                    im.SenderConnection.Disconnect("Unknown package received");
                                }
                            }
                            break;
                        default:
                            {
                                Log("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
                            }
                            break;
                    }
                    _gameServer.Recycle(im);
                }
                Thread.Sleep(1);
            }

            Reset();
        }

        private int GetFreePort()
        {
            int port = 15000;
            while (_portsInUse.Contains(port))
                port += 1;
            return port;
        }

        public void Shutdown()
        {
            if (_gameServer == null)
                return;

            _shouldStop = true;
            _gameServer.Shutdown("Server closed");
        }

        private void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}][GameServerHandler] {1}", time, text));
        }
    }
}
