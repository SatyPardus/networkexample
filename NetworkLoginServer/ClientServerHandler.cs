﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public partial class ClientServerHandler
    {
        private bool _isRunning = false;
        private bool _shouldStop = false;
        private NetServer _clientServer;
        private int _lastID = 1;

        private Dictionary<long, Client> _clientList = new Dictionary<long, Client>();

        public bool DebugEnabled = true;
        public bool IsRunning { get { return _isRunning; } }

        public void Initialize()
        {
            NetPeerConfiguration config = new NetPeerConfiguration("clientserver");
            config.ConnectionTimeout = 10;
            config.Port = 7788;
            config.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);

            _clientServer = new NetServer(config);
            _clientServer.Start();

            new Thread(new ThreadStart(ListenToMessages)).Start();
        }

        private void Reset()
        {
            _shouldStop = false;
            _isRunning = false;
            _clientServer = null;
        }

        private void ListenToMessages()
        {
            _isRunning = true;

            while (!_shouldStop)
            {
                NetIncomingMessage im;
                while ((im = _clientServer.ReadMessage()) != null)
                {
                    // handle incoming message
                    switch (im.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            if (DebugEnabled)
                                Log(im.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            {
                                NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                                string reason = im.ReadString();

                                if (DebugEnabled)
                                    Log(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);
                                Client client = null;
                                if (!_clientList.TryGetValue(im.SenderConnection.RemoteUniqueIdentifier, out client))
                                {
                                    break;
                                }

                                if (status == NetConnectionStatus.Connected)
                                {
                                    SendClientConnected(client);
                                    Log(string.Format("{0} ({1}) connected to the server", client.Username, client.ID));
                                }
                                else if (status == NetConnectionStatus.Disconnected)
                                {
                                    SendClientDisconnected(client);
                                    _clientList.Remove(im.SenderConnection.RemoteUniqueIdentifier);
                                    Log(string.Format("{0} ({1}) disconnected from the server", client.Username, client.ID));
                                }
                            }
                            break;
                        case NetIncomingMessageType.ConnectionApproval:
                            {
                                string username = "";
                                if (!im.ReadString(out username))
                                {
                                    // When the client has no username, kick him.
                                    im.SenderConnection.Deny();
                                    break;
                                }
                                if (username.Length < 2)
                                {
                                    im.SenderConnection.Deny("Username to short");
                                    break;
                                }
                                if (CheckUsernameExists(username))
                                {
                                    im.SenderConnection.Deny("Username already in use");
                                    break;
                                }

                                int id = _lastID++;

                                _clientList.Add(im.SenderConnection.RemoteUniqueIdentifier, new Client(im.SenderConnection, id, username));

                                NetOutgoingMessage userList = _clientServer.CreateMessage();
                                userList.Write(id);
                                userList.Write(username);
                                userList.Write(_clientList.Count - 1);
                                foreach (Client client in _clientList.Values)
                                {
                                    if (id == client.ID)
                                        continue;

                                    userList.Write(client.ID);
                                    userList.Write(client.Username);
                                }
                                im.SenderConnection.Approve(userList);
                            }
                            break;
                        case NetIncomingMessageType.Data:
                            {
                                byte typeid = 0;
                                if (im.ReadByte(out typeid))
                                {
                                    Client client = null;
                                    if (!_clientList.TryGetValue(im.SenderConnection.RemoteUniqueIdentifier, out client))
                                    {
                                        break;
                                    }

                                    switch ((MessageType)typeid)
                                    {
                                        case MessageType.ChatMessage:
                                            string message = "";
                                            if(im.ReadString(out message))
                                            {
                                                ReceiveChatMessage(client, message);
                                            }
                                            break;
                                    }
                                }
                            }
                            break;
                        default:
                            Log("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
                            break;
                    }
                    _clientServer.Recycle(im);
                }
                Thread.Sleep(1);
            }

            Reset();
        }

        private bool CheckUsernameExists(string username)
        {
            foreach (Client client in _clientList.Values)
            {
                if (client.Username.Equals(username, StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        public void Shutdown()
        {
            if (_clientServer == null)
                return;

            _shouldStop = true;
            _clientServer.Shutdown("Server closed");
        }

        private void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}][ClientServerHandler] {1}", time, text));
        }
    }
}
