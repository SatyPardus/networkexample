﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public class GameServer
    {
        private int _id;
        private int _port;
        private int _runningGames;
        private int _connectedClients;

        public int ID { get { return _id; } }
        public int Port { get { return _port; } }
        public int RunningGames { get { return _runningGames; } }
        public int ConnectedClients { get { return _connectedClients; } }

        public GameServer(int id, int port)
        {
            _id = id;
            _port = port;

            _runningGames = 0;
            _connectedClients = 0;
        }

        public void UpdateServer(int runningGames, int connectedClients)
        {
            _runningGames = runningGames;
            _connectedClients = connectedClients;
        }
    }
}
