﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientServerHandler clientserver = new ClientServerHandler();
            GameServerHandler gameServer = new GameServerHandler(clientserver);
            clientserver.Initialize();
            gameServer.Initialize();

            string input = "";
            while((gameServer.IsRunning && clientserver.IsRunning) || input != "quit")
            {
                input = Console.ReadLine();

                if(input.Equals("debug", StringComparison.CurrentCultureIgnoreCase))
                {
                    gameServer.DebugEnabled = !gameServer.DebugEnabled;
                    clientserver.DebugEnabled = !clientserver.DebugEnabled;
                    Log(string.Format("Debug mode has been {0}", (gameServer.DebugEnabled ? "enabled" : "disabled")));
                }
            }

            gameServer.Shutdown();
            clientserver.Shutdown();
        }

        private static void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}] {1}", time, text));
        }
    }
}
