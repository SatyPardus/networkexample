﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkLoginServer
{
    public partial class ClientServerHandler
    {
        #region "Messages to send"
        public void SendClientConnected(Client client)
        {
            NetOutgoingMessage clientConnectedMessage = _clientServer.CreateMessage();
            clientConnectedMessage.Write((byte)MessageType.ClientConnected);
            clientConnectedMessage.Write(client.ID);
            clientConnectedMessage.Write(client.Username);
            _clientServer.SendToAll(clientConnectedMessage, client.Connection, NetDeliveryMethod.ReliableOrdered, 0);
        }

        public void SendClientDisconnected(Client client)
        {
            NetOutgoingMessage clientDisconnectedMessage = _clientServer.CreateMessage();
            clientDisconnectedMessage.Write((byte)MessageType.ClientDisconnected);
            clientDisconnectedMessage.Write(client.ID);
            _clientServer.SendToAll(clientDisconnectedMessage, NetDeliveryMethod.ReliableOrdered);
        }
        #endregion

        #region "Messages to receive"
        public void ReceiveChatMessage(Client client, string message)
        {
            NetOutgoingMessage chatMessage = _clientServer.CreateMessage();
            chatMessage.Write((byte)MessageType.ChatMessage);
            if (client != null)
                chatMessage.Write(client.ID);
            else
                chatMessage.Write(0);
            chatMessage.Write(message);
            _clientServer.SendToAll(chatMessage, NetDeliveryMethod.ReliableOrdered);
        }
        #endregion
    }
}
