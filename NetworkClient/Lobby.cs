﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkClient
{
    public partial class Lobby : Form
    {
        public Lobby(List<Client> userlist)
        {
            this.FormClosed += Lobby_Close;

            Program.LoginServer.OnClientConnected += OnClientConnected;
            Program.LoginServer.OnClientDisconnected += OnClientDisconnected;
            Program.LoginServer.OnChatMessageReceived += OnChatMessageReceived;

            InitializeComponent();

            foreach(Client client in userlist)
            {
                listPlayers.Items.Add(client);
            }
        }

        private void Lobby_Close(object sender, FormClosedEventArgs e)
        {
            Program.LoginServer.OnClientConnected -= OnClientConnected;
            Program.LoginServer.OnClientDisconnected -= OnClientDisconnected;
            Program.LoginServer.OnChatMessageReceived -= OnChatMessageReceived;
        }

        private void Lobby_Load(object sender, EventArgs e)
        {
            Program.LoginServer.OnDisconnected += OnDisconnected;
        }

        private void OnChatMessageReceived(string sender, string message)
        {
            chatBox.Text += string.Format("\n{0}: {1}", sender, message);
        }

        private void OnClientDisconnected(Client client)
        {
            foreach(Client c in listPlayers.Items)
            {
                if(c == client)
                {
                    listPlayers.Items.Remove(client);
                    break;
                }
            }
        }

        private void OnClientConnected(Client client)
        {
            listPlayers.Items.Add(client);
        }

        private void OnDisconnected(string reason)
        {
            Close();
        }

        private void btnSendChat_Click(object sender, EventArgs e)
        {
            Program.LoginServer.SendChatMessage(txtChat.Text);
            txtChat.Text = "";
        }
    }
}
