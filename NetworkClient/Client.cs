﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkClient
{
    public class Client
    {
        private int _id;
        private string _username;

        public int ID { get { return _id; } }
        public string Username { get { return _username; } }

        public Client(int id, string username)
        {
            _id = id;
            _username = username;
        }

        public override string ToString()
        {
            return _username;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Client) || ((Client)obj).ID != _id)
                return false;
            return true;
        }

        public bool Equals(Client c)
        {
            if (c == null || c.ID != _id)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }
}
