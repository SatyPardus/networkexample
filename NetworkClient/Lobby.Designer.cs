﻿namespace NetworkClient
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listPlayers = new System.Windows.Forms.ListBox();
            this.lblOnlinePlayers = new System.Windows.Forms.Label();
            this.chatBox = new System.Windows.Forms.RichTextBox();
            this.txtChat = new System.Windows.Forms.TextBox();
            this.btnChallengePlayer = new System.Windows.Forms.Button();
            this.btnSendChat = new System.Windows.Forms.Button();
            this.lblChat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listPlayers
            // 
            this.listPlayers.FormattingEnabled = true;
            this.listPlayers.Location = new System.Drawing.Point(461, 23);
            this.listPlayers.Name = "listPlayers";
            this.listPlayers.Size = new System.Drawing.Size(120, 290);
            this.listPlayers.TabIndex = 0;
            // 
            // lblOnlinePlayers
            // 
            this.lblOnlinePlayers.AutoSize = true;
            this.lblOnlinePlayers.Location = new System.Drawing.Point(458, 7);
            this.lblOnlinePlayers.Name = "lblOnlinePlayers";
            this.lblOnlinePlayers.Size = new System.Drawing.Size(74, 13);
            this.lblOnlinePlayers.TabIndex = 1;
            this.lblOnlinePlayers.Text = "Online Players";
            // 
            // chatBox
            // 
            this.chatBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chatBox.Location = new System.Drawing.Point(4, 23);
            this.chatBox.Name = "chatBox";
            this.chatBox.ReadOnly = true;
            this.chatBox.Size = new System.Drawing.Size(451, 265);
            this.chatBox.TabIndex = 2;
            this.chatBox.Text = "Double click a player or select and press \"Challenge\" to play.";
            // 
            // txtChat
            // 
            this.txtChat.Location = new System.Drawing.Point(4, 292);
            this.txtChat.Name = "txtChat";
            this.txtChat.Size = new System.Drawing.Size(291, 20);
            this.txtChat.TabIndex = 3;
            // 
            // btnChallengePlayer
            // 
            this.btnChallengePlayer.Location = new System.Drawing.Point(380, 290);
            this.btnChallengePlayer.Name = "btnChallengePlayer";
            this.btnChallengePlayer.Size = new System.Drawing.Size(75, 23);
            this.btnChallengePlayer.TabIndex = 4;
            this.btnChallengePlayer.Text = "Challenge";
            this.btnChallengePlayer.UseVisualStyleBackColor = true;
            // 
            // btnSendChat
            // 
            this.btnSendChat.Location = new System.Drawing.Point(301, 290);
            this.btnSendChat.Name = "btnSendChat";
            this.btnSendChat.Size = new System.Drawing.Size(75, 23);
            this.btnSendChat.TabIndex = 5;
            this.btnSendChat.Text = "Send";
            this.btnSendChat.UseVisualStyleBackColor = true;
            this.btnSendChat.Click += new System.EventHandler(this.btnSendChat_Click);
            // 
            // lblChat
            // 
            this.lblChat.AutoSize = true;
            this.lblChat.Location = new System.Drawing.Point(1, 7);
            this.lblChat.Name = "lblChat";
            this.lblChat.Size = new System.Drawing.Size(74, 13);
            this.lblChat.TabIndex = 6;
            this.lblChat.Text = "Online Players";
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 314);
            this.Controls.Add(this.lblChat);
            this.Controls.Add(this.btnSendChat);
            this.Controls.Add(this.btnChallengePlayer);
            this.Controls.Add(this.txtChat);
            this.Controls.Add(this.chatBox);
            this.Controls.Add(this.lblOnlinePlayers);
            this.Controls.Add(this.listPlayers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Lobby";
            this.Text = "Lobby";
            this.Load += new System.EventHandler(this.Lobby_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listPlayers;
        private System.Windows.Forms.Label lblOnlinePlayers;
        private System.Windows.Forms.RichTextBox chatBox;
        private System.Windows.Forms.TextBox txtChat;
        private System.Windows.Forms.Button btnChallengePlayer;
        private System.Windows.Forms.Button btnSendChat;
        private System.Windows.Forms.Label lblChat;
    }
}