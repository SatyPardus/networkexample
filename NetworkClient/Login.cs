﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkClient
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();

            this.FormClosing += Login_Close;

            Program.LoginServer.OnConnected += OnConnected;
            Program.LoginServer.OnDisconnected += OnDisconnected;
        }

        private void Login_Close(object sender, EventArgs e)
        {
            Program.LoginServer.OnConnected -= OnConnected;
            Program.LoginServer.OnDisconnected -= OnDisconnected;
            Program.LoginServer.Shutdown();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            Program.LoginServer.Initialize();
            Program.Debug = new DebugForm();
            Program.Debug.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Program.LoginServer.Connect(txtUsername.Text);
        }

        private void OnDisconnected(string reason)
        {
            if (reason != "Disconnected")
                MessageBox.Show("You got disconnected: " + reason, "Login Server");
            Show();
        }

        private void OnConnected(List<Client> userlist)
        {
            Lobby lobby = new Lobby(userlist);
            lobby.Show();
            lobby.FormClosed += OnLobbyClosed;
            Hide();
        }

        private void OnLobbyClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Program.LoginServer.Shutdown();
                Show();
            }
        }

        private void LoginServerUpdate_Tick(object sender, EventArgs e)
        {
            Program.LoginServer.Update();
        }

        private void btnDebug_Click(object sender, EventArgs e)
        {
            Program.Debug.Show();
        }
    }
}
