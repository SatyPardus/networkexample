﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkClient
{
    static class Program
    {
        public static DebugForm Debug;
        public static LoginServerHandler LoginServer;
        public static GameServerHandler GameServer;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoginServer = new LoginServerHandler();
            GameServer = new GameServerHandler();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }
    }
}
