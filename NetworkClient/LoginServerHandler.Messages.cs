﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkClient
{
    public partial class LoginServerHandler
    {
        #region "Messages to send"
        public void SendChatMessage(string message)
        {
            NetOutgoingMessage chatMessage = _loginServer.CreateMessage();
            chatMessage.Write((byte)MessageType.ChatMessage);
            chatMessage.Write(message);
            _loginServer.SendMessage(chatMessage, NetDeliveryMethod.ReliableOrdered);
        }
        #endregion

        #region "Messages to receive"
        private void ReceiveClientConnected(int id, string username)
        {
            Client client = new Client(id, username);

            _clientList.Add(id, client);
            RaiseOnClientConnectedEvent(client);
        }

        private void ReceiveClientDisconnected(int id)
        {
            Client client = null;

            if (!_clientList.TryGetValue(id, out client))
                return;

            _clientList.Remove(id);
            RaiseOnClientDisconnectedEvent(client);
        }

        private void ReceiveChatMessage(int userid, string message)
        {
            string username = "Server";
            if (userid != 0)
            {
                Client client = null;
                if (_clientList.TryGetValue(userid, out client))
                    username = client.Username;
                else if (userid == _myClient.ID)
                    username = _myClient.Username;
            }

            RaiseOnChatMessageReceivedEvent(username, message);
        }
        #endregion
    }
}
