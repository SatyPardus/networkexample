﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkClient
{
    public delegate void OnConnectedHandler(List<Client> userlist);
    public delegate void OnDisconnectedHandler(string reason);
    public delegate void OnClientConnectedHandler(Client client);
    public delegate void OnClientDisconnectedHandler(Client client);
    public delegate void OnChatMessageReceivedHandler(string sender, string message);

    public partial class LoginServerHandler
    {
        public event OnConnectedHandler OnConnected;
        public event OnDisconnectedHandler OnDisconnected;
        public event OnClientConnectedHandler OnClientConnected;
        public event OnClientDisconnectedHandler OnClientDisconnected;
        public event OnChatMessageReceivedHandler OnChatMessageReceived;

        public void RaiseOnConnectedEvent(List<Client> userlist)
        {
            if (OnConnected != null)
            {
                OnConnected(userlist);
            }
        }

        public void RaiseOnDisconnectedEvent(string reason)
        {
            if (OnDisconnected != null)
            {
                OnDisconnected(reason);
            }
        }

        public void RaiseOnClientConnectedEvent(Client client)
        {
            if (OnClientConnected != null)
            {
                OnClientConnected(client);
            }
        }

        public void RaiseOnClientDisconnectedEvent(Client client)
        {
            if (OnClientDisconnected != null)
            {
                OnClientDisconnected(client);
            }
        }

        public void RaiseOnChatMessageReceivedEvent(string sender, string message)
        {
            if (OnChatMessageReceived != null)
            {
                OnChatMessageReceived(sender, message);
            }
        }
    }
}
