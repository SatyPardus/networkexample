﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkClient
{
    public class GameServerHandler
    {
        private Client _myClient;
        private NetClient _gameServer;
        private Dictionary<int, Client> _clientList = new Dictionary<int, Client>();

        public Client MyClient { get { return _myClient; } }

        public void Initialize()
        {
            NetPeerConfiguration config = new NetPeerConfiguration("clientserver");
            _gameServer = new NetClient(config);
        }

        public void Connect(string username, int port)
        {
            NetOutgoingMessage loginMessage = _gameServer.CreateMessage();
            loginMessage.Write(username);

            _gameServer.Start();
            _gameServer.Connect("127.0.0.1", port, loginMessage);
        }

        public void Update()
        {
            NetIncomingMessage im;
            while ((im = _gameServer.ReadMessage()) != null)
            {
                // handle incoming message
                switch (im.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        Log(im.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        {
                            NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                            string reason = im.ReadString();

                            Log(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);

                            if (status == NetConnectionStatus.Connected)
                            {
                                int myid = im.SenderConnection.RemoteHailMessage.ReadInt32();
                                string myname = im.SenderConnection.RemoteHailMessage.ReadString();
                                _myClient = new Client(myid, myname);

                                //int users = im.SenderConnection.RemoteHailMessage.ReadInt32();
                                //for (int i = 0; i < users; i++)
                                //{
                                //    int userid = im.SenderConnection.RemoteHailMessage.ReadInt32();
                                //    string username = im.SenderConnection.RemoteHailMessage.ReadString();
                                //    _clientList.Add(userid, new Client(userid, username));
                                //}

                                //RaiseOnConnectedEvent(new List<Client>(_clientList.Values));
                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                _myClient = null;
                                _clientList.Clear();

                                //RaiseOnDisconnectedEvent(reason);
                            }
                        }
                        break;
                    case NetIncomingMessageType.Data:
                        {
                            //MessageType type = (MessageType)im.ReadByte();
                            //switch (type)
                            //{
                            //    case MessageType.ClientConnected:
                            //        {
                            //            int id = im.ReadInt32();
                            //            string username = im.ReadString();

                            //            ReceiveClientConnected(id, username);
                            //        }
                            //        break;
                            //    case MessageType.ClientDisconnected:
                            //        {
                            //            int id = im.ReadInt32();

                            //            ReceiveClientDisconnected(id);
                            //        }
                            //        break;
                            //    case MessageType.ChatMessage:
                            //        {
                            //            int id = im.ReadInt32();
                            //            string message = im.ReadString();

                            //            ReceiveChatMessage(id, message);
                            //        }
                            //        break;
                            //}
                        }
                        break;
                    default:
                        Log("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
                        break;
                }
                _gameServer.Recycle(im);
            }
        }

        public void Shutdown()
        {
            if (_gameServer == null)
                return;

            _gameServer.Shutdown("Disconnected");
        }

        private void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}][GameServerHandler] {1}", time, text));
        }
    }
}
