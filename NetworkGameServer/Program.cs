﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetworkGameServer
{
    class Program
    {
        static void Main(string[] args)
        {
            LoginServerHandler loginServer = new LoginServerHandler();
            loginServer.Initialize();

            while(loginServer.IsRunning)
            {
                Thread.Sleep(500);
            }
        }
    }
}
