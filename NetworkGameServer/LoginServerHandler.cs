﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetworkGameServer
{
    public partial class LoginServerHandler
    {
        private bool _isRunning = false;
        private bool _shouldStop = false;
        private NetClient _loginServer;
        private ClientServerHandler _clientServerHandler = new ClientServerHandler();

        public bool IsRunning { get { return _isRunning; } }

        public void Initialize()
        {
            NetPeerConfiguration config = new NetPeerConfiguration("gameserver");
            _loginServer = new NetClient(config);
            _loginServer.Start();
            _loginServer.Connect("127.0.0.1", 7777);

            new Thread(new ThreadStart(ListenToMessages)).Start();
        }

        private void Reset()
        {
            _shouldStop = false;
            _isRunning = false;
            _loginServer = null;
        }

        private void ListenToMessages()
        {
            _isRunning = true;

            while (!_shouldStop)
            {
                NetIncomingMessage im;
                while ((im = _loginServer.ReadMessage()) != null)
                {
                    // handle incoming message
                    switch (im.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            Log(im.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                            string reason = im.ReadString();

                            Log(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);

                            if (status == NetConnectionStatus.Connected)
                            {
                                if (_clientServerHandler.IsRunning)
                                    _clientServerHandler.Shutdown();
                                while (_clientServerHandler.IsRunning) { } // Wait for server to stop

                                int port = im.SenderConnection.RemoteHailMessage.ReadInt32();
                                _clientServerHandler.Initialize(port);
                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                if (_clientServerHandler.IsRunning)
                                    _clientServerHandler.Shutdown();
                                _shouldStop = true;
                            }
                            break;
                        case NetIncomingMessageType.Data:

                            break;
                        default:
                            Log("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
                            break;
                    }
                    _loginServer.Recycle(im);
                }
            }

            Reset();
        }

        public void Shutdown()
        {
            if (_loginServer == null)
                return;

            _shouldStop = true;
            _loginServer.Shutdown("Server closed");
        }

        private void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}][LoginServerHandler] {1}", time, text));
        }
    }
}
