﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetworkGameServer
{
    public class ClientServerHandler
    {
        private bool _isRunning = false;
        private bool _shouldStop = false;
        private NetServer _clientServer;

        public bool IsRunning { get { return _isRunning; } }

        public void Initialize(int port)
        {
            NetPeerConfiguration config = new NetPeerConfiguration("gameserver");
            config.ConnectionTimeout = 10;
            config.Port = port;
            config.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);

            _clientServer = new NetServer(config);
            _clientServer.Start();

            new Thread(new ThreadStart(ListenToMessages)).Start();
        }

        private void Reset()
        {
            _shouldStop = false;
            _isRunning = false;
            _clientServer = null;
        }

        private void ListenToMessages()
        {
            _isRunning = true;

            while (!_shouldStop)
            {
                NetIncomingMessage im;
                while ((im = _clientServer.ReadMessage()) != null)
                {
                    // handle incoming message
                    switch (im.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            Log(im.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                            string reason = im.ReadString();

                            Log(NetUtility.ToHexString(im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);

                            if (status == NetConnectionStatus.Connected)
                            {

                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                
                            }
                            break;
                        case NetIncomingMessageType.ConnectionApproval:

                            break;
                        case NetIncomingMessageType.Data:

                            break;
                        default:
                            Log("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
                            break;
                    }
                    _clientServer.Recycle(im);
                }
            }

            Reset();
        }

        public void Shutdown()
        {
            if (_clientServer == null)
                return;

            _shouldStop = true;
            _clientServer.Shutdown("Server closed");
        }

        private void Log(string text)
        {
            string time = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("[{0}][ClientServerHandler] {1}", time, text));
        }
    }
}
